<?php

use Alura\Doctrine\Entity\Course;
use Alura\Doctrine\Entity\Phone;
use Alura\Doctrine\Entity\Student;
use Alura\Doctrine\Helper\EntityManagerCreator;

require_once __DIR__ . '/../vendor/autoload.php';

$entityManager = EntityManagerCreator::createEntityManager();

$studentRepository = $entityManager->getRepository(Student::class);

$studentList = $studentRepository->findAll();

/* @var Student[] $studentList */
foreach ($studentList as $student) {
    echo "ID: $student->id NOME: $student->name\n\n";
    echo "Telefones:\n";

    /* @var Phone $phone */
    echo implode(', ', $student->phones()->map(fn($phone) => $phone->phone)->toArray());

    echo PHP_EOL;

    echo "Cursos:\n";

    /* @var Course $course */
    echo implode(', ', $student->courses()->map(fn($course) => $course->name)->toArray());
    echo PHP_EOL;
}

$student = $studentRepository->find(3);

echo "NOME: $student->name\n\n";

echo "ALUNOS TOTAL: {$studentRepository->count([])}\n";


$student = $studentRepository->findOneBy(['name' => 'floaty']);

echo "NOME: $student->name";