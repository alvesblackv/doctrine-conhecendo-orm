<?php

use Alura\Doctrine\Entity\Phone;
use Alura\Doctrine\Entity\Student;
use Alura\Doctrine\Helper\EntityManagerCreator;

require_once __DIR__ . '/../vendor/autoload.php';

$entityManager = EntityManagerCreator::createEntityManager();

$student = new Student('Marcelo com telefone new');
$student->addPhone(new Phone('(11) 1111-1111'));
$student->addPhone(new Phone('(22) 2222-2222'));

$entityManager->persist($student);

$entityManager->flush();