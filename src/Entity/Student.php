<?php

namespace Alura\Doctrine\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;

#[Entity]
class Student
{
    #[Id]
    #[GeneratedValue]
    #[Column]
    public  int $id;

    #[OneToMany(mappedBy: 'student',
        targetEntity: Phone::class,
        cascade: ["persist", "remove"]
    )]
    public Collection $phones;

    #[ManyToMany(targetEntity: Course::class, inversedBy: 'students')]
    public Collection $courses;
    public function __construct(
        #[Column]
        public string $name
    )
    {
        $this->phones = new ArrayCollection();
        $this->courses = new ArrayCollection();
    }

    public function addPhone(Phone $phone): void
    {
        $this->phones->add($phone);
        $phone->setStudent($this);
    }

    public function phones(): Collection
    {
        return $this->phones;
    }

    public function addCourse(Course $course): void
    {
        $this->courses->add($course);
        $course->setStudent($this);
    }
    public function courses(): Collection
    {
        return $this->courses;
    }
}